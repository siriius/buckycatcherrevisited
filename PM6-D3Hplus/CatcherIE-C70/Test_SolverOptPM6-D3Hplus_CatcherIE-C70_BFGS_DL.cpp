#include <iostream>
#include <vector>
#include "math/SolverPackage.hpp"
#include "MNDOd.hpp"

int main() {
  BFGSd solve(4,6);

  Molecule molec("../db/geom/pm6-d3hplus/4a_catcher_ie_c70.xyz",0,1,"C1");
  BSet basis(molec,"pm6");
  
  PM6 electron(basis,molec,"0","D3H+");
  electron.Calculate(0);

  double enthresh = 5e-8;
  double gthresh = 5e-5;
  xTBthresholds("extreme",enthresh,gthresh);
  std::cout << "thresholds used: " << enthresh << " " << gthresh << std::endl;
  SolverOpt(electron,solve,4,0,enthresh,gthresh);
  
  std::cout << "optimized geometry: " << std::endl;
  matrixE optgeom = electron.Geometry();
  optgeom.Print();
  
  std::cout << std::setprecision(10);
  std::cout << "Energy = " << electron.getEnergy(1) << std::endl;

  return 0;
}
