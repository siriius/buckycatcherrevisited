#include <iostream>
#include "Molecule.hpp"
#include "math/MatrixPackage.hpp"

int main() {
  Molecule C60("../db/geom/pm6-d3hplus/4a_catcher_ie_c70.xyz",0,1,"C1");
  
  std::vector<double> inA = C60.InertiaEigenvalues();
  
  std::cout << std::setprecision(10) << std::scientific;
  
  std::cout << "inertia" << std::endl;
  for (size_t idx = 0; idx < 3; ++idx) {
    std::cout << inA[idx] << std::endl;
  }
  
  return 0;
}
